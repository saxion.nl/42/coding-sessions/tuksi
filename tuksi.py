from lark import Lark, Transformer
from unidecode import unidecode
import os

class SentenceTransformer(Transformer):
    def greeting(self, name):
        return name

    def product(self, words):
        return " ".join(list(words))
    
    def person(self, name):
        return name
    
    def command(self, items):
        # [product, [person]]
        person = items[1][0].value
        product = items[0].replace("'", "")
        os.system(f"/usr/bin/say -vEllen OK {person} [[slnc 200]], I have charged you for a {product}")
        return f"{product} for {person}"

def main():
    with open('tuksi.lark') as grammar:
        tuksi_parser = Lark(grammar.read(), start='sentence')

    inputs = [
        "hey Taxi I want to order a milky way for Ivo",
        "Taxi, I want to order a milky way for Ivo",
        "Taxi, I want to order a milky way for Jüüp",
        "Taxi, order B'tween Hazelnoot Karamel for Leander"
    ]

    for sentence in inputs:
        tree = tuksi_parser.parse(unidecode(sentence))
        print(tree.pretty())
        print(SentenceTransformer().transform(tree))

if __name__ == "__main__":
    main()

